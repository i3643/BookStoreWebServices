How to set up?
I created this project by using h2 database, so there is no need to import database. All information about users and books are located in import.sql file.
When applocation run they are automatically inserted.

Main url - http://localhost:8086/bookstore 
APIs UserController 1 (login) http://localhost:8086/bookstore/user/login (POST) 2 (signUp) http://localhost:8086/bookstore/user/signUp (POST) 
BookController 1 (getbooklist) http://localhost:8086/bookstore/books/bookList (GET) 
2 (getbooklistById) http://localhost:8086/bookstore/books/id/{id} (GET) 
3 (getBookByName) http://localhost:8086/bookstore/books/name/{name} (GET) 
4 (getBooksByPublisherNameAndSurname) http://localhost:8086/bookstore/books/publisher (GET) 
5 (addBook) http://localhost:8086/bookstore/books/addBook (POST) 
6 (updateBook) http://localhost:8086/bookstore/books/updateBookByPublisher (PUT)
You can check them in any explorer (Except usercontroller 2 and bookcontroller 5,6 you need postman) 
I did not include security (I jave just learnt it and it is little bit confusing for now).

swagger url : http://localhost:8086/bookstore/swagger-ui.html
h2 console : http://localhost:8086/bookstore/h2-console
jdbc : jdbc:h2:mem:bookstore
User Name: Murad
Password : 12345
