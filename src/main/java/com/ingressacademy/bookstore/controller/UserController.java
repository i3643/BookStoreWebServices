package com.ingressacademy.bookstore.controller;

import com.ingressacademy.bookstore.request.RequestUser;
import com.ingressacademy.bookstore.response.RespStatus;
import com.ingressacademy.bookstore.response.RespStatusList;
import com.ingressacademy.bookstore.service.UserService;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
@Tag(name="user-controller")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/login")
    public RespStatusList login(@RequestParam String userName, @RequestParam String password){
        return userService.checkLogin(userName,password);
    }

    @PostMapping("/signUp")
    public RespStatusList signUp(@RequestBody RequestUser reqUser){
        return userService.signUp(reqUser);
    }

}
