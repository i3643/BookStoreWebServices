package com.ingressacademy.bookstore.service;

import com.ingressacademy.bookstore.request.RequestBook;
import com.ingressacademy.bookstore.response.RespBook;
import com.ingressacademy.bookstore.response.RespStatusList;
import com.ingressacademy.bookstore.response.Response;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface BookService {
    Response<List<RespBook>> getBookList();

    Response<RespBook> getBookById(Long id);

    Response<List<RespBook>> getBookByName(String name,Integer pageNumber,Integer limit);

    Response<List<RespBook>> getBooksByPublisherNameAndSurname(String name, String surName);

    RespStatusList addBook(RequestBook reqBook);

    RespStatusList updateLesson(RequestBook reqBook);
}
