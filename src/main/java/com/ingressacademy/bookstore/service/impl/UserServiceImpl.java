package com.ingressacademy.bookstore.service.impl;

import com.ingressacademy.bookstore.entity.User;
import com.ingressacademy.bookstore.entity.UserType;
import com.ingressacademy.bookstore.enums.EnumAvailableStatus;
import com.ingressacademy.bookstore.exception.BookStoreException;
import com.ingressacademy.bookstore.exception.ExceptionConstants;
import com.ingressacademy.bookstore.repository.UserRepository;
import com.ingressacademy.bookstore.repository.UserTypeRepository;
import com.ingressacademy.bookstore.request.RequestUser;
import com.ingressacademy.bookstore.response.RespStatus;
import com.ingressacademy.bookstore.response.RespStatusList;
import com.ingressacademy.bookstore.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserTypeRepository userTypeRepository;
    @Override
    public RespStatusList checkLogin(String userName, String password) {
        RespStatusList respStatusList = new RespStatusList();
        try {

            User user = userRepository.findByUsernameAndPasswordAndStatus(userName, password, EnumAvailableStatus.ACTIVE.getValue());
            if (user == null) {
                throw new BookStoreException(ExceptionConstants.USER_NOT_FOUND, "Username and Passoword are wrong !");
            }
            respStatusList.setStatus(RespStatus.getSuccessMessage());
        } catch (BookStoreException ex) {
            respStatusList.setStatus(new RespStatus(ex.getCode(), ex.getMessage()));
        } catch (Exception ex) {
            ex.printStackTrace();
            respStatusList.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }
        return respStatusList;
    }

    @Override
    public RespStatusList signUp(RequestUser reqUser) {
        RespStatusList respStatusList = new RespStatusList();
        try {
            if(reqUser.getUserTypeId() == null || reqUser.getEmail() == null || reqUser.getUsername() == null || reqUser.getPassword() == null
                    || reqUser.getName() ==null || reqUser.getSurname() ==null){
                throw new BookStoreException(ExceptionConstants.INVALID_REQUEST_DATA,"Invalid Request Data !");
            }
            Optional<UserType> userType = userTypeRepository.findById(reqUser.getUserTypeId());

            if(userRepository.findByEmailAndStatus(reqUser.getEmail(),EnumAvailableStatus.ACTIVE.getValue())!=null){
                throw new BookStoreException(ExceptionConstants.INVALID_REQUEST_DATA,"Email Has Already Registered !");
            }
            if(reqUser.getEmail().contains("@") != true || reqUser.getEmail().length() < 8){
                throw new BookStoreException(ExceptionConstants.INVALID_EMAIL, "Invalid email !");
            }
            Date date = new Date();
            User user = new User();
            user.setName(reqUser.getName());
            user.setSurname(reqUser.getSurname());
            user.setEmail(reqUser.getEmail());
            user.setUsername(reqUser.getPassword());
            user.setPassword(reqUser.getPassword());
            user.setUserType(userType.get());
            user.setStatus(1);
            user.setInsertDate(date);
            respStatusList.setStatus(RespStatus.getSuccessMessage());
            userRepository.save(user);
        } catch (BookStoreException ex) {
            respStatusList.setStatus(new RespStatus(ex.getCode(), ex.getMessage()));
        } catch (Exception ex) {
            ex.printStackTrace();
            respStatusList.setStatus(new RespStatus(ExceptionConstants.INTERNAL_EXCEPTION, "Internal Exception"));
        }

        return respStatusList;
    }
}
