package com.ingressacademy.bookstore.service;

import com.ingressacademy.bookstore.request.RequestUser;
import com.ingressacademy.bookstore.response.RespStatus;
import com.ingressacademy.bookstore.response.RespStatusList;
import org.springframework.stereotype.Service;

@Service
public interface UserService {
    RespStatusList checkLogin(String userName, String password);

    RespStatusList signUp(RequestUser reqUser);
}
