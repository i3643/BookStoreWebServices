package com.ingressacademy.bookstore.exception;

import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class BookStoreException extends RuntimeException {

    private Integer code;

    public BookStoreException(Integer code, String message) {
         super(message);
         this.code = code;
    }

    public Integer getCode() {
        return code;
    }

}
