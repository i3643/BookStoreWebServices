package com.ingressacademy.bookstore.repository;


import com.ingressacademy.bookstore.entity.User;
import com.ingressacademy.bookstore.entity.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsernameAndPasswordAndStatus(String userName, String password, Integer status);

    User findByNameAndSurnameAndStatus(String name, String surname, Integer status);

    User findByIdAndUserTypeAndStatus(Long id, UserType userType, Integer status);

    User findByEmailAndStatus(String email, Integer status);


}
