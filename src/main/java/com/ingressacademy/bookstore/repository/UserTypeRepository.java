package com.ingressacademy.bookstore.repository;

import com.ingressacademy.bookstore.entity.UserType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserTypeRepository  extends JpaRepository<UserType,Long> {

    Optional<UserType> findById (Long id);
    List<UserType> findAllByStatus(Integer status);
}
