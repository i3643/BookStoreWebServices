package com.ingressacademy.bookstore.request;

import com.ingressacademy.bookstore.entity.UserType;
import lombok.Data;
import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import java.util.Date;

@Data
public class RequestUser {

    private String name;

    private String surname;

    private String email;

    private String username;

    private String password;

    private Long userTypeId;


}
